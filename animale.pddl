(define (domain animale-strips)
(:predicates 
	(animal ?a)
	(condition-ok ?a)
	(condition-notok ?a)
	(condition-wild ?a)
	(condition-notwild ?a)
	(vet ?v)
	(shelter-wild ?sw)
	(shelter-notwild ?snw)
	(transfer-animal-to-shelter-wild ?a ?sw)
	(transfer-animal-to-shelter-notwild ?a ?snw)
	(transfer-animal-to-vet ?a ?v)
	(home ?h)
	(transfer-animal-to-home ?a ?h)
	(wildness ?w)
	(transfer-animal-to-wildness ?a ?w))

(:action move-animal-to-shelter-wild
	:parameters (?a1 ?sw)
	:precondition ( and (animal ?a1) (condition-ok ?a1) (condition-wild ?a1) (shelter-wild ?sw))
	:effect (transfer-animal-to-shelter-wild ?a1 ?sw))

(:action move-animal-to-shelter-notwild
	:parameters (?a1 ?snw)
	:precondition ( and (animal ?a1) (condition-ok ?a1) (condition-notwild ?a1) (shelter-notwild ?snw))
	:effect (transfer-animal-to-shelter-notwild ?a1 ?snw))


(:action move-animal-to-vet
	:parameters (?a1 ?v1)
	:precondition ( and (animal ?a1) (condition-notok ?a1) (vet ?v1))
	:effect (transfer-animal-to-vet ?a1 ?v1))

(:action move-animal-to-home
	:parameters (?a1 ?h1)
	:precondition ( and (animal ?a1) (condition-notwild ?a1)(condition-ok ?a1)(home ?h1))
	:effect  (transfer-animal-to-home ?a1 ?h1))

(:action move-animal-to-wildness
	:parameters (?a1 ?w1)
	:precondition (and (animal ?a1) (condition-wild ?a1) (condition-ok ?a1)(wildness ?w1))
	:effect (transfer-animal-to-wildness ?a1 ?w1)))
